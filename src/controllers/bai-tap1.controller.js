const { baiTap1Service } = require('../services');
const catchAsync = require('../utils/catchAsync');

//  Liệt kê danh sách các lớp của khoa, thông tin cần Malop, TenLop, MaKhoa SELECT * FROM Lop
const cau1 = catchAsync(async (req, res) => {
  const result = await baiTap1Service.cau1();
  res.send(result);
});

// Lập danh sách sinh viên gồm: MaSV, HoTen, HocBong SELECT MaSV, Hoten, HocBong FROM SinhVien
const cau2 = catchAsync(async (req, res) => {
  const result = await baiTap1Service.cau2();
  res.send(result);
});

//  Lập danh sách sinh viên có học bổng. Danh sách cần MaSV, Nu, HocBong
const cau3 = catchAsync(async (req, res) => {
  const result = await baiTap1Service.cau3();
  res.send(result);
});

// Lập danh sách sinh viên nữ. Danh sách cần các thuộc tính của quan hệ sinhvien SELECT * FROM SinhVien WHERE Nu =Yes
const cau4 = catchAsync(async (req, res) => {
  const result = await baiTap1Service.cau4();
  res.send(result);
});

// Lập danh sách sinh viên có họ ‘Trần’ SELECT * FROM SinhVien WHERE HoTen Like ‘Trần *’
const cau5 = catchAsync(async (req, res) => {
  const result = await baiTap1Service.cau5();
  res.send(result);
});

// Lập danh sách sinh viên nữ có học bổng SELECT * FROM SinhVien WHERE Nu=Yes AND HocBong>0
const cau6 = catchAsync(async (req, res) => {
  const result = await baiTap1Service.cau6();
  res.send(result);
});

// Lập danh sách sinh viên nữ hoặc danh sách sinh viên có học bổng SELECT * FROM SinhVien WHERE Nu=Yes OR HocBong>0
const cau7 = catchAsync(async (req, res) => {
  const result = await baiTap1Service.cau7();
  res.send(result);
});

// Cau 8:  Lập danh sách sinh viên có năm sinh từ 1978 đến 1985.
// Danh sách cần các thuộc tính của quan hệ SinhVien SELECT * FROM SinhVien WHERE YEAR(NgaySinh) BETWEEN 1978 AND 1985
const cau8 = catchAsync(async (req, res) => {
  const result = await baiTap1Service.cau8();
  res.send(result);
});

// Câu 9: Liệt kê danh sách sinh viên được sắp xếp tăng dần theo MaSV SELECT * FROM SinhVien ORDER BY MaSV
const cau9 = catchAsync(async (req, res) => {
  const result = await baiTap1Service.cau9();
  res.send(result);
});

// Câu 10: Liệt kê danh sách sinh viên được sắp xếp giảm dần theo HocBong
const cau10 = catchAsync(async (req, res) => {
  const result = await baiTap1Service.cau10();
  res.send(result);
});

// Ví du11: Lập danh sách sinh viên có điểm thi môn CSDL>=8
// SELECT SinhVien.MaSV, HoTen, Nu, NgaySinh, DiemThi FROM SinhVien INNER JOIN
// KetQua ON SinhVien.MaSV = KetQua.MaSV WHERE MaMH = ‘CSDL’ AND DiemThi>=8
const cau11 = catchAsync(async (req, res) => {
  const result = await baiTap1Service.cau11();
  res.send(result);
});

// Ví du 12: Lập danh sách sinh viên có học bổng của khoa CNTT. Thông tin cần: MaSV, HoTen, HocBong,TenLop
// SELECT MaSV, HoTen, HocBong, TenLop FROM Lop INNER JOIN SinhVien ON Lop.MaLop=SinhVien.MaLop WHERE HocBong>0 AND MaKhoa =’CNTT’
const cau12 = catchAsync(async (req, res) => {
  const result = await baiTap1Service.cau12();
  res.send(result);
});

//  Ví du 13: Lập danh sách sinh viên có học bổng của khoa CNTT. Thông tin cần: MaSV, HoTen, HocBong,TenLop, TenKhoa
// SELECT MaSV, HoTen, HocBong, TenLop,TenKhoa FROM ((Lop INNER JOIN SinhVien ON Lop.MaLop=SinhVien.MaLop)
// INNER JOIN Khoa ON Khoa.MaKhoa=Lop.MaKhoa) WHERE HocBong>0 AND Khoa.MaKhoa =’CNTT’
const cau13 = catchAsync(async (req, res) => {
  const result = await baiTap1Service.cau13();
  res.send(result);
});

const cau14 = catchAsync(async (req, res) => {
  const result = await baiTap1Service.cau14();
  res.send(result);
});

const cau15 = catchAsync(async (req, res) => {
  const result = await baiTap1Service.cau15();
  res.send(result);
});

const cau16 = catchAsync(async (req, res) => {
  const result = await baiTap1Service.cau16();
  res.send(result);
});

const cau17 = catchAsync(async (req, res) => {
  const result = await baiTap1Service.cau17();
  res.send(result);
});

const cau18 = catchAsync(async (req, res) => {
  const result = await baiTap1Service.cau18();
  res.send(result);
});

const cau19 = catchAsync(async (req, res) => {
  const result = await baiTap1Service.cau19();
  res.send(result);
});

const cau20 = catchAsync(async (req, res) => {
  const result = await baiTap1Service.cau20();
  res.send(result);
});

const cau21 = catchAsync(async (req, res) => {
  const result = await baiTap1Service.cau21();
  res.send(result);
});

const cau22 = catchAsync(async (req, res) => {
  const result = await baiTap1Service.cau22();
  res.send(result);
});

const cau23 = catchAsync(async (req, res) => {
  const result = await baiTap1Service.cau23();
  res.send(result);
});

const cau24 = catchAsync(async (req, res) => {
  const result = await baiTap1Service.cau24();
  res.send(result);
});

const cau25 = catchAsync(async (req, res) => {
  const result = await baiTap1Service.cau25();
  res.send(result);
});

module.exports = {
  cau1,
  cau2,
  cau3,
  cau4,
  cau5,
  cau6,
  cau7,
  cau8,
  cau9,
  cau10,
  cau11,
  cau12,
  cau13,
  cau14,
  cau15,
  cau16,
  cau17,
  cau18,
  cau19,
  cau20,
  cau21,
  cau22,
  cau23,
  cau24,
  cau25,
};
