const Lop = require('../models/lop.model');
const SinhVien = require('../models/sinh-vien.model');
const KetQua = require('../models/ket-qua.model');
const MonHoc = require('../models/mon-hoc.model');
const Khoa = require('../models/khoa.model');

const cau1 = async () => {
  const result = await Lop.find({});
  return result;
};

//  Lập danh sách sinh viên gồm: MaSV, HoTen, HocBong SELECT MaSV, Hoten, HocBong FROM SinhVien
const cau2 = async () => {
  const result = await SinhVien.find({}).select('_id ho_ten hoc_bong');
  return result;
};

// Lập danh sách sinh viên có học bổng. Danh sách cần MaSV, Nu, HocBong
const cau3 = async () => {
  const result = await SinhVien.find({}).select('_id nu hoc_bong').where('hoc_bong').gt(0);
  //  const result = await SinhVien.find({
  //    hoc_bong: { $gt: 0 },
  //  }).select({ nu: 1, hoc_bong: 1 });
  return result;
};

// Lập danh sách sinh viên nữ. Danh sách cần các thuộc tính của quan hệ sinhvien SELECT * FROM SinhVien WHERE Nu =Yes
const cau4 = async () => {
  //   const result = await SinhVien.find().where('nu').equals(true);
  const result = await SinhVien.find({
    nu: true,
  });
  return result;
};

//  Lập danh sách sinh viên có họ ‘Trần’ SELECT * FROM SinhVien WHERE HoTen Like ‘Trần *’
const cau5 = async () => {
  const result = await SinhVien.find({ ho_ten: /^Trần/ });
  /*
  const result = await SinhVien.find({
    ho_ten: { $regex: /^Trần/ },
  });
  */
  return result;
};

// Lập danh sách sinh viên nữ có học bổng SELECT * FROM SinhVien WHERE Nu=Yes AND HocBong>0
const cau6 = async () => {
  //   const result = await SinhVien.find().where('nu').equals(true).where('hoc_bong').gt(0);
  const result = await SinhVien.find({
    nu: true,
    hoc_bong: { $gt: 0 },
  });
  return result;
};

// Lập danh sách sinh viên nữ hoặc danh sách sinh viên có học bổng SELECT * FROM SinhVien WHERE Nu=Yes OR HocBong>0
const cau7 = async () => {
  const result = await SinhVien.find({
    $or: [{ nu: true }, { hoc_bong: { $gt: 0 } }],
  });
  return result;
};

// Cau 8:  Lập danh sách sinh viên có năm sinh từ 1978 đến 1985.
// Danh sách cần các thuộc tính của quan hệ SinhVien SELECT * FROM SinhVien WHERE YEAR(NgaySinh) BETWEEN 1978 AND 1985
const cau8 = async () => {
  const result = await SinhVien.find({
    ngay_sinh: {
      $gte: new Date(1978, 1, 1),
      $lt: new Date(1986, 1, 1),
    },
  });
  return result;
};

// Câu 9: Liệt kê danh sách sinh viên được sắp xếp tăng dần theo MaSV SELECT * FROM SinhVien ORDER BY MaSV
const cau9 = async () => {
  const result = await SinhVien.find().sort({ _id: 1 });
  return result;
};

// Câu 10: Liệt kê danh sách sinh viên được sắp xếp giảm dần theo HocBong
const cau10 = async () => {
  const result = await SinhVien.find().sort({ hoc_bong: -1 });
  return result;
};

// Ví du 11: Lập danh sách sinh viên có điểm thi môn CSDL>=8
// SELECT SinhVien.MaSV, HoTen, Nu, NgaySinh, DiemThi FROM SinhVien INNER JOIN
// KetQua ON SinhVien.MaSV = KetQua.MaSV WHERE MaMH = ‘CSDL’ AND DiemThi>=8
const cau11 = async () => {
  const result = await KetQua.aggregate([
    {
      $lookup: {
        from: SinhVien.collection.name,
        localField: 'maSV',
        foreignField: '_id',
        as: 'bangDiems',
      },
    },
    {
      $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ['$bangDiems', 0] }, '$$ROOT'] } },
    },
    {
      $project: {
        bangDiems: 0,
      },
    },
    {
      $lookup: {
        from: MonHoc.collection.name,
        localField: 'maMH',
        foreignField: '_id',
        as: 'ketQuaMonHocs',
      },
    },
    {
      $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ['$ketQuaMonHocs', 0] }, '$$ROOT'] } },
    },
    {
      $project: {
        ketQuaMonHocs: 0,
      },
    },
    {
      $match: {
        tenMH: 'CSDL',
        diemThi: { $gte: 8 },
      },
    },
    {
      $project: {
        _id: 1,
        ho_ten: 1,
        nu: 1,
        ngay_sinh: 1,
        diemThi: 1,
      },
    },
  ]);
  return result;
};

// Ví du 12: Lập danh sách sinh viên có học bổng của khoa CNTT. Thông tin cần: MaSV, HoTen, HocBong,TenLop
// SELECT MaSV, HoTen, HocBong, TenLop FROM Lop INNER JOIN SinhVien ON Lop.MaLop=SinhVien.MaLop WHERE HocBong>0 AND MaKhoa =’CNTT’
const cau12 = async () => {
  const result = await SinhVien.aggregate([
    {
      $lookup: {
        from: Lop.collection.name,
        localField: 'ma_lop',
        foreignField: '_id',
        as: 'lop',
      },
    },
    {
      $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ['$lop', 0] }, '$$ROOT'] } },
    },
    {
      $project: {
        lop: 0,
      },
    },
    {
      $lookup: {
        from: Khoa.collection.name,
        localField: 'ma_khoa',
        foreignField: '_id',
        as: 'khoa_lop',
      },
    },
    {
      $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ['$khoa_lop', 0] }, '$$ROOT'] } },
    },
    {
      $project: {
        khoa_lop: 0,
      },
    },
    {
      $match: {
        ten_khoa: 'CNTT',
        hoc_bong: { $gt: 0 },
      },
    },
    {
      $project: {
        _id: 1,
        ho_ten: 1,
        hoc_bong: 1,
        ten_lop: 1,
      },
    },
  ]);
  return result;
};

//  Ví du 13: Lập danh sách sinh viên có học bổng của khoa CNTT. Thông tin cần: MaSV, HoTen, HocBong,TenLop, TenKhoa
// SELECT MaSV, HoTen, HocBong, TenLop,TenKhoa FROM ((Lop INNER JOIN SinhVien ON Lop.MaLop=SinhVien.MaLop)
// INNER JOIN Khoa ON Khoa.MaKhoa=Lop.MaKhoa) WHERE HocBong>0 AND Khoa.MaKhoa =’CNTT’
const cau13 = async () => {
  const result = await SinhVien.aggregate([
    {
      $lookup: {
        from: Lop.collection.name,
        localField: 'ma_lop',
        foreignField: '_id',
        as: 'lop',
      },
    },
    {
      $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ['$lop', 0] }, '$$ROOT'] } },
    },
    {
      $project: {
        lop: 0,
      },
    },
    {
      $lookup: {
        from: Khoa.collection.name,
        localField: 'ma_khoa',
        foreignField: '_id',
        as: 'khoa_lop',
      },
    },
    {
      $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ['$khoa_lop', 0] }, '$$ROOT'] } },
    },
    {
      $project: {
        khoa_lop: 0,
      },
    },
    {
      $match: {
        ten_khoa: 'CNTT',
        hoc_bong: { $gt: 0 },
      },
    },
    {
      $project: {
        _id: 1,
        ho_ten: 1,
        hoc_bong: 1,
        ten_lop: 1,
        ten_khoa: 1,
      },
    },
  ]);
  return result;
};

// Câu 14: Cho biết số sinh viên của mỗi lớp
// SELECT Lop.MaLop, TenLop, Count(MaSV) as SLsinhvien FROM Lop
// INNER JOIN SinhVien ON Lop.MaLop = SinhVien.MaLop GROUP BY Lop.MaLop, TenLop
const cau14 = async () => {
  // const result = await SinhVien.aggregate([
  //   {
  //     $lookup: {
  //       from: Lop.collection.name,
  //       localField: 'ma_lop',
  //       foreignField: '_id',
  //       as: 'sinhvien_lop',
  //     },
  //   },
  //   {
  //     $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ['$sinhvien_lop', 0] }, '$$ROOT'] } },
  //   },
  //   {
  //     $project: {
  //       sinhvien_lop: 0,
  //     },
  //   },
  //   {
  //     $group: {
  //       _id: '$ma_lop',
  //       ten_lop: { $first: '$ten_lop' },
  //       SLsinhvien: { $sum: 1 },
  //     },
  //   },
  // ]);
  const result = await SinhVien.aggregate([
    {
      $group: {
        _id: '$ma_lop',
        SLsinhvien: { $sum: 1 },
      },
    },
    {
      $lookup: {
        from: Lop.collection.name,
        localField: '_id',
        foreignField: '_id',
        as: 'sinhvien_lop',
      },
    },
    {
      $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ['$sinhvien_lop', 0] }, '$$ROOT'] } },
    },
    {
      $project: {
        sinhvien_lop: 0,
      },
    },
    {
      $project: {
        _id: 1,
        ten_lop: 1,
        SLsinhvien: 1,
      },
    },
  ]);
  return result;
};

// Câu 15: Cho biết số lượng sinh viên của mỗi khoa.
// SELECT Khoa.MaKhoa, TenKhoa, Count(MaSV) as SLsinhvien
// FROM ((Khoa INNER JOIN Lop ON Khoa.Makhoa = Lop.MaKhoa)INNER JOIN SinhVien ON Lop.MaLop = SinhVien.MaLop)
// GROUP BY Khoa.MaKhoa, TenKhoa
const cau15 = async () => {
  const result = await SinhVien.aggregate([
    {
      $lookup: {
        from: Lop.collection.name,
        localField: 'ma_lop',
        foreignField: '_id',
        as: 'sinhvien_lop',
      },
    },
    {
      $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ['$sinhvien_lop', 0] }, '$$ROOT'] } },
    },
    {
      $project: {
        sinhvien_lop: 0,
      },
    },
    {
      $group: {
        _id: '$ma_khoa',
        SLsinhvien: { $sum: 1 },
      },
    },
    {
      $lookup: {
        from: Khoa.collection.name,
        localField: '_id',
        foreignField: '_id',
        as: 'sinhvien_khoa',
      },
    },
    {
      $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ['$sinhvien_khoa', 0] }, '$$ROOT'] } },
    },
    {
      $project: {
        _id: 1,
        ten_khoa: 1,
        SLsinhvien: 1,
      },
    },
  ]);
  return result;
};

// Câu 16: Cho biết số lượng sinh viên nữ của mỗi khoa.
// SELECT Khoa.MaKhoa, TenKhoa, Count(MaSV) as SLsinhvien FROM ((SinhVien INNER JOIN Lop ON Lop.MaLop = SinhVien.MaLop)
// INNER JOIN khoa ON KHOA.makhoa = Lop.makhoa) WHERE Nu=Yes GROUP BY Khoa.MaKhoa, TenKhoa
const cau16 = async () => {
  const result = await SinhVien.aggregate([
    {
      $match: {
        nu: true,
      },
    },
    {
      $lookup: {
        from: Lop.collection.name,
        localField: 'ma_lop',
        foreignField: '_id',
        as: 'sinhvien_lop',
      },
    },
    {
      $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ['$sinhvien_lop', 0] }, '$$ROOT'] } },
    },
    {
      $project: {
        sinhvien_lop: 0,
      },
    },
    {
      $group: {
        _id: '$ma_khoa',
        SLsinhvien: { $sum: 1 },
      },
    },
    {
      $lookup: {
        from: Khoa.collection.name,
        localField: '_id',
        foreignField: '_id',
        as: 'sinhvien_khoa',
      },
    },
    {
      $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ['$sinhvien_khoa', 0] }, '$$ROOT'] } },
    },
    {
      $project: {
        _id: 1,
        ten_khoa: 1,
        SLsinhvien: 1,
      },
    },
  ]);
  return result;
};

// Câu 17: Cho biết tổng tiền học bổng của mỗi lớp
// SELECT Lop.MaLop, TenLop, Sum(HocBong) as TongHB FROM (Lop INNER JOIN SinhVien ON Lop.MaLop = SinhVien.MaLop)
// GROUP BY Lop.MaLop, TenLop
const cau17 = async () => {
  const result = await SinhVien.aggregate([
    {
      $group: {
        _id: '$ma_lop',
        tongHB: { $sum: '$hoc_bong' },
      },
    },
    {
      $lookup: {
        from: Lop.collection.name,
        localField: '_id',
        foreignField: '_id',
        as: 'sinhvien_lop',
      },
    },
    {
      $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ['$sinhvien_lop', 0] }, '$$ROOT'] } },
    },
    {
      $project: {
        sinhvien_lop: 0,
      },
    },
    {
      $project: {
        _id: 1,
        ten_lop: 1,
        tongHB: 1,
      },
    },
  ]);
  return result;
};

// Câu 18: Cho biết tổng số tiền học bổng của mỗi khoa
// SELECT Khoa.MaKhoa, TenKhoa, Sum(HocBong) as TongHB
// FROM ((Khoa INNER JOIN Lop ON Khoa.Makhoa = Lop.MaKhoa)INNER JOIN SinhVien ON Lop.MaLop = SinhVien.MaLop) GROUP BY Khoa.MaKhoa, TenKhoa
const cau18 = async () => {
  const result = await SinhVien.aggregate([
    {
      $lookup: {
        from: Lop.collection.name,
        localField: 'ma_lop',
        foreignField: '_id',
        as: 'sinhvien_lop',
      },
    },
    {
      $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ['$sinhvien_lop', 0] }, '$$ROOT'] } },
    },
    {
      $project: {
        sinhvien_lop: 0,
      },
    },
    {
      $group: {
        _id: '$ma_khoa',
        tongHB: { $sum: '$hoc_bong' },
      },
    },
    {
      $lookup: {
        from: Khoa.collection.name,
        localField: '_id',
        foreignField: '_id',
        as: 'sinhvien_khoa',
      },
    },
    {
      $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ['$sinhvien_khoa', 0] }, '$$ROOT'] } },
    },
    {
      $project: {
        _id: 1,
        ten_khoa: 1,
        tongHB: 1,
      },
    },
  ]);
  return result;
};

// Câu 19: Lập danh sánh những khoa có nhiều hơn 100 sinh viên.
// Danh sách cần: MaKhoa, TenKhoa, Soluong
// SELECT Khoa.MaKhoa, TenKhoa, Count(MaSV) as SLsinhvien FROM ((Khoa INNER JOIN Lop ON Khoa.Makhoa = Lop.MaKhoa)
// INNER JOIN SinhVien ON Lop.MaLop = SinhVien.MaLop) GROUP BY Khoa.MaKhoa, TenKhoa HAVING Count(MaSV) >100
const cau19 = async () => {
  const result = await SinhVien.aggregate([
    {
      $lookup: {
        from: Lop.collection.name,
        localField: 'ma_lop',
        foreignField: '_id',
        as: 'sinhvien_lop',
      },
    },
    {
      $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ['$sinhvien_lop', 0] }, '$$ROOT'] } },
    },
    {
      $project: {
        sinhvien_lop: 0,
      },
    },
    {
      $group: {
        _id: '$ma_khoa',
        SLsinhvien: { $sum: 1 },
      },
    },
    {
      $match: {
        SLsinhvien: { $gt: 100 },
      },
    },
    {
      $lookup: {
        from: Khoa.collection.name,
        localField: '_id',
        foreignField: '_id',
        as: 'sinhvien_khoa',
      },
    },
    {
      $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ['$sinhvien_khoa', 0] }, '$$ROOT'] } },
    },
    {
      $project: {
        _id: 1,
        ten_khoa: 1,
        SLsinhvien: 1,
      },
    },
  ]);
  return result;
};

// Câu 20: Lập danh sánh những khoa có nhiều hơn 50 sinh viên nữ.
// Danh sách cần: MaKhoa, TenKhoa, Soluong SELECT Khoa.MaKhoa, TenKhoa, Count(MaSV) as SLsinhvien FROM
// ((Khoa INNER JOIN Lop ON Khoa.Makhoa = Lop.MaKhoa)INNER JOIN SinhVien ON Lop.MaLop = SinhVien.MaLop)
// WHERE Nu=Yes GROUP BY Khoa.MaKhoa, TenKhoa HAVING Count(MaSV)>=50
const cau20 = async () => {
  const result = await SinhVien.aggregate([
    {
      $match: {
        nu: true,
      },
    },
    {
      $lookup: {
        from: Lop.collection.name,
        localField: 'ma_lop',
        foreignField: '_id',
        as: 'sinhvien_lop',
      },
    },
    {
      $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ['$sinhvien_lop', 0] }, '$$ROOT'] } },
    },
    {
      $project: {
        sinhvien_lop: 0,
      },
    },
    {
      $group: {
        _id: '$ma_khoa',
        SLsinhvien: { $sum: 1 },
      },
    },
    {
      $match: {
        SLsinhvien: { $gt: 100 },
      },
    },
    {
      $lookup: {
        from: Khoa.collection.name,
        localField: '_id',
        foreignField: '_id',
        as: 'sinhvien_khoa',
      },
    },
    {
      $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ['$sinhvien_khoa', 0] }, '$$ROOT'] } },
    },
    {
      $project: {
        _id: 1,
        ten_khoa: 1,
        SLsinhvien: 1,
      },
    },
  ]);
  return result;
};

// Câu 21: Lập danh sách những khoa có tổng tiền học bổng >=1000000.
// SELECT Khoa.MaKhoa, TenKhoa, Sum(HocBong) as TongHB FROM ((Khoa INNER JOIN Lop ON Khoa.Makhoa = Lop.MaKhoa)
// INNER JOIN SinhVien ON Lop.MaLop = SinhVien.MaLop) GROUP BY Khoa.MaKhoa, TenKhoa HAVING Sum(HocBong)>= 1000000
const cau21 = async () => {
  const result = await SinhVien.aggregate([
    {
      $lookup: {
        from: Lop.collection.name,
        localField: 'ma_lop',
        foreignField: '_id',
        as: 'sinhvien_lop',
      },
    },
    {
      $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ['$sinhvien_lop', 0] }, '$$ROOT'] } },
    },
    {
      $project: {
        sinhvien_lop: 0,
      },
    },
    {
      $group: {
        _id: '$ma_khoa',
        tongHB: { $sum: '$hoc_bong' },
      },
    },
    {
      $match: {
        tongHB: { $gt: 1000000 },
      },
    },
    {
      $lookup: {
        from: Khoa.collection.name,
        localField: '_id',
        foreignField: '_id',
        as: 'sinhvien_khoa',
      },
    },
    {
      $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ['$sinhvien_khoa', 0] }, '$$ROOT'] } },
    },
    {
      $project: {
        _id: 1,
        ten_khoa: 1,
        tongHB: 1,
      },
    },
  ]);
  return result;
};

// Câu22: Lập danh sách sinh viên có học bổng cao nhất
// SELECT SinhVien.* FROM SinhVien WHERE HocBong>= ALL(SELECT HocBong From Sinhvien)
const cau22 = async () => {
  const result = SinhVien.find().sort({ hoc_bong: -1 }).limit(1);
  return result;
};

// Câu 23: Lập danh sách sinh viên có điểm thi môn CSDL cao nhất
// SELECT SinhVien.MaSV, HoTen, DiemThi FROM SinhVien INNER JOIN KetQua ON SinhVien.MaSV = KetQua.MaSV
// WHERE KetQua.MaMH= ‘CSDL’ AND DiemThi>= ALL(SELECT DiemThi FROM KetQua WHERE MaMH = ‘CSDL’)
const cau23 = async () => {
  const result = await KetQua.aggregate([
    {
      $lookup: {
        from: SinhVien.collection.name,
        localField: 'maSV',
        foreignField: '_id',
        as: 'bangDiems',
      },
    },
    {
      $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ['$bangDiems', 0] }, '$$ROOT'] } },
    },
    {
      $project: {
        bangDiems: 0,
      },
    },
    {
      $lookup: {
        from: MonHoc.collection.name,
        localField: 'maMH',
        foreignField: '_id',
        as: 'ketQuaMonHocs',
      },
    },
    {
      $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ['$ketQuaMonHocs', 0] }, '$$ROOT'] } },
    },
    {
      $project: {
        ketQuaMonHocs: 0,
      },
    },
    {
      $project: {
        _id: 1,
        ho_ten: 1,
        diemThi: 1,
      },
    },
    {
      $sort: {
        diemThi: -1,
      },
    },
    {
      $limit: 1,
    },
  ]);
  return result;
};

// Câu 24: Lập danh sách những sinh viên không có điểm thi môn CSDL.
// SELECT SinhVien.MaSV, HoTen, DiemThi,MaMH FROM SinhVien
// INNER JOIN KetQua ON SinhVien.MaSV = KetQua.MaSV WHERE SinhVien.MaSV NOT In (Select MaSV From KetQua Where MaMH=’CSDL’)
const cau24 = async () => {
  const temp = await KetQua.aggregate([
    {
      $lookup: {
        from: MonHoc.collection.name,
        localField: 'maMH',
        foreignField: '_id',
        as: 'KetQuaMonHocs',
      },
    },
    {
      $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ['$KetQuaMonHocs', 0] }, '$$ROOT'] } },
    },
    {
      $project: {
        KetQuaMonHocs: 0,
      },
    },
    {
      $match: {
        tenMH: 'CSDL',
      },
    },
    {
      $project: {
        _id: 0,
        maSV: 1,
      },
    },
  ]);
  const result = await SinhVien.find({ _id: { $nin: temp.map((x) => x.maSV) } });
  return result;
};

//  Cho biết những khoa nào có nhiều sinh viên nhất
// SELECT Khoa.MaKhoa, TenKhoa, Count([MaSV]) AS SoLuongSV FROM (Khoa INNER JOIN Lop ON Khoa.MaKhoa = Lop.MaKhoa)
// INNER JOIN SinhVien ON Lop.MaLop = SinhVien.MaLop GROUP BY Khoa.MaKhoa, Khoa.TenKhoa HaVing Count(MaSV)
// >=All(Select Count(MaSV) From ((SinhVien Inner Join Lop On Lop.Malop=SinhVien.Malop)Inner Join Khoa On Khoa.MaKhoa = Lop.MaKhoa )
// Group By Khoa.Makhoa)
const cau25 = async () => {
  const result = await SinhVien.aggregate([
    {
      $lookup: {
        from: Lop.collection.name,
        localField: 'ma_lop',
        foreignField: '_id',
        as: 'sinhvien_lop',
      },
    },
    {
      $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ['$sinhvien_lop', 0] }, '$$ROOT'] } },
    },
    {
      $project: {
        sinhvien_lop: 0,
      },
    },
    {
      $group: {
        _id: '$ma_khoa',
        SLsinhvien: { $sum: 1 },
      },
    },
    {
      $lookup: {
        from: Khoa.collection.name,
        localField: '_id',
        foreignField: '_id',
        as: 'sinhvien_khoa',
      },
    },
    {
      $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ['$sinhvien_khoa', 0] }, '$$ROOT'] } },
    },
    {
      $project: {
        _id: 1,
        ten_khoa: 1,
        SLsinhvien: 1,
      },
    },
    {
      $sort: { SLsinhvien: -1 },
    },
    {
      $limit: 1,
    },
  ]);
  return result;
};

module.exports = {
  cau1,
  cau2,
  cau3,
  cau4,
  cau5,
  cau6,
  cau7,
  cau8,
  cau9,
  cau10,
  cau11,
  cau12,
  cau13,
  cau14,
  cau15,
  cau16,
  cau17,
  cau18,
  cau19,
  cau20,
  cau21,
  cau22,
  cau23,
  cau24,
  cau25,
};
