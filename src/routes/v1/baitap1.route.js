const express = require('express');
const baiTap1Controller = require('../../controllers/bai-tap1.controller');

const router = express.Router();

router.get('/cau1', baiTap1Controller.cau1);
router.get('/cau2', baiTap1Controller.cau2);
router.get('/cau3', baiTap1Controller.cau3);
router.get('/cau4', baiTap1Controller.cau4);
router.get('/cau5', baiTap1Controller.cau5);
router.get('/cau6', baiTap1Controller.cau6);
router.get('/cau7', baiTap1Controller.cau7);
router.get('/cau8', baiTap1Controller.cau8);
router.get('/cau9', baiTap1Controller.cau9);
router.get('/cau10', baiTap1Controller.cau10);
router.get('/cau11', baiTap1Controller.cau11);
router.get('/cau12', baiTap1Controller.cau12);
router.get('/cau13', baiTap1Controller.cau13);
router.get('/cau14', baiTap1Controller.cau14);
router.get('/cau15', baiTap1Controller.cau15);
router.get('/cau16', baiTap1Controller.cau16);
router.get('/cau17', baiTap1Controller.cau17);
router.get('/cau18', baiTap1Controller.cau18);
router.get('/cau19', baiTap1Controller.cau19);
router.get('/cau20', baiTap1Controller.cau20);
router.get('/cau21', baiTap1Controller.cau21);
router.get('/cau22', baiTap1Controller.cau22);
router.get('/cau23', baiTap1Controller.cau23);
router.get('/cau24', baiTap1Controller.cau24);
router.get('/cau25', baiTap1Controller.cau25);

module.exports = router;
