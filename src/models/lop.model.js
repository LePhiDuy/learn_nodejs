const mongoose = require('mongoose');

const { Schema } = mongoose;

// Lop(MaLop, TenLop, MaKhoa)
const LopSchema = new Schema(
  {
    tenLop: { type: String },
    maKhoa: {
      type: Schema.Types.ObjectId,
      ref: 'Khoa',
    },
  },
  {
    timestamps: true,
  }
);

const Lop = mongoose.model('Lop', LopSchema);

module.exports = Lop;
