const mongoose = require('mongoose');

const { Schema } = mongoose;

//  KetQua(MaSV, MaMH, DiemThi)
const KetQuaSchema = new Schema(
  {
    maSV: {
      type: Schema.Types.ObjectId,
      ref: 'SinhVien',
      unique: true,
    },
    maMH: {
      type: Schema.Types.ObjectId,
      ref: 'MonHoc',
      require: true,
    },
    diemThi: {
      type: Number,
      min: 0,
      max: 10,
    },
  },
  {
    timestamps: true,
  }
);

const KetQua = mongoose.model('KetQua', KetQuaSchema);

module.exports = KetQua;
