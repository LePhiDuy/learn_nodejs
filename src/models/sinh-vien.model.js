const mongoose = require('mongoose');

const { Schema } = mongoose;

//  MaSV, HoTen, Nu, NgaySinh, MaLop, HocBong, Tinh
const SinhVienSchema = new Schema(
  {
    hoTen: { type: String, required: true },
    nu: { type: Boolean },
    ngaySinh: { type: Date },
    maLop: {
      type: Schema.Types.ObjectId,
      ref: 'Lop',
    },
    hocBong: { type: Number },
    tinh: { type: String },
  },
  {
    timestamps: true,
  }
);

const SinhVien = mongoose.model('SinhVien', SinhVienSchema);

module.exports = SinhVien;
