const mongoose = require('mongoose');

const { Schema } = mongoose;

//  MonHoc(MaMH, TenMH, SoTiet)
const MonHocSchema = new Schema(
  {
    tenMH: { type: String },
    soTiet: { type: Number },
  },
  {
    timestamps: true,
  }
);

const MonHoc = mongoose.model('MonHoc', MonHocSchema);

module.exports = MonHoc;
