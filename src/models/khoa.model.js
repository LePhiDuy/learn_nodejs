const mongoose = require('mongoose');

const { Schema } = mongoose;

//  Khoa(MaKhoa, TenKhoa, SoCBGD)
const KhoaSchema = new Schema(
  {
    tenKhoa: { type: String },
    soCBGD: { type: Number },
  },
  {
    timestamps: true,
  }
);

const Khoa = mongoose.model('Khoa', KhoaSchema);

module.exports = Khoa;
